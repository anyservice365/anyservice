/**
 * 
 */
package com.anyservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author DHIRAJ
 *
 */
@Controller
public class UserController {
	
	@RequestMapping(value="/seeker/dashboard", method = RequestMethod.GET)
	public ModelAndView seekerDashboard(){
		return new ModelAndView("seeker");
	}
	
	@RequestMapping(value="/provider/dashboard", method = RequestMethod.GET)
	public ModelAndView providerDashboard(){
		return new ModelAndView("provider");
	}
	
}
