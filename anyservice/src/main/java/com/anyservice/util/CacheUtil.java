package com.anyservice.util;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;

import com.anyservice.model.UserInfo;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;

public class CacheUtil {
public static final String USER_CACHE = "com.mms.core.user";
	
	public static final String MAX_USERS = "com.core.app";
	


	private static final Serializable NO_OF_USERS = "R8OhcqD5mKboI++aSdjiZX5QmrYwYPsiOGwRLUvoLqU=";


	public static CacheManager cacheMgr = null;



	public static UserInfo getUserInfo(String userKey) {
		Ehcache cache = getCache(USER_CACHE);
		Element userElement = cache.get(userKey);
		if (userElement != null) {
			return (UserInfo) userElement.getObjectValue();
		} else {
			return null;
		}
	}

	public static void putUserInfo(String userKey, UserInfo uInfo) {
		Ehcache cache = getCache(USER_CACHE);
		cache.put(new Element(userKey, uInfo));

	}

	public static void updateUserKey(String userId, String userKey) {
		Ehcache cache = getCache(USER_CACHE);
		Element userElement = cache.get(userId);
		if (userElement != null) {
			String key = (String) userElement.getObjectValue();
			cache.remove(key); // Invalidating the older session;
			/*Logger.getLogger(AuthenticationServiceImpl.class).info(
					"Older Session for User " + userId + " invalidated and user logged in again!");*/
		}
		userElement = new Element(userId, userKey);
		cache.put(userElement);
	}

	private static Ehcache getCache(String cacheType) {

		if (cacheMgr == null) {

			URL resource = CacheUtil.class.getResource("/ehcache.xml");

			try {
				cacheMgr = CacheManager.create(resource.openStream());
			} catch (CacheException | IOException e) {
				throw new RuntimeException("CACHE_CREATION_FAILED", e);
			}
			// cacheMgr = CacheManager.create();
		}

		Ehcache cache = null;
		if (cacheMgr != null) {
			cache = cacheMgr.getEhcache(cacheType);
			if (cache == null) {
				CacheConfiguration config = new CacheConfiguration(cacheType, 10000);
				config.setTimeToIdleSeconds(0);
				config.setEternal(true);
				config.setTimeToLiveSeconds(0);
				config.setOverflowToDisk(false);
				cache = new Cache(config);
				cacheMgr.addCache(cache);
			}
		}
		return cache;
	}

	public static void removeUserInfo(String key) {
		Ehcache cache = getCache(USER_CACHE);
		cache.remove(key);
	}

	
	/*
	 * public static void main(String[] args) {
	 * 
	 * String ss=new String(); String ssNew=new String();
	 * 
	 * 
	 * ss ="Dbar";
	 * 
	 * Ehcache cache =getCache(MMSCoreMessage.USER_CACHE);
	 * 
	 * cache.put(new Element("dbar", ss));
	 * 
	 * Element e=cache.get("dbar"); ssNew= (String) e.getObjectValue();
	 * 
	 * System.out.println(ssNew);
	 * 
	 * 
	 * }
	 */
	
	public static Integer getUsersInSession() {
		Ehcache cache = getCache(MAX_USERS);
		Element userElement = cache.get(NO_OF_USERS);
		if(userElement!=null){
		return (Integer) userElement.getObjectValue();
		}else{
			return 0;
		}
		
	}

	public static void setUsersInSession(Integer noOfusers) {
		Ehcache cache = getCache(MAX_USERS);
		cache.put(new Element(NO_OF_USERS, noOfusers));

	}
	


}
