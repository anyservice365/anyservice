/**
 * 
 */
package com.anyservice.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.anyservice.dao.BaseDao;
import com.anyservice.model.BaseEntity;

@Repository
public class BaseDaoImpl implements BaseDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public <T extends BaseEntity> T findByBusinessKey(Class<T> entity,
			Map<String, Object> param) {
		if (param == null) {
			throw new RuntimeException("Invalid Key Value pair");
		}
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				entity);
		for (Entry<String, Object> key : param.entrySet()) {
			criteria.add(Restrictions.eq(key.getKey(), key.getValue()));
		}
		List<T> entityList = criteria.list();
		if (entityList.size() > 1) {
			throw new RuntimeException("No unique Result Found");
		} else if (entityList.size() > 0) {
			return entityList.get(0);
		} else {
			return null;
		}
	}

	@Override
	public <T extends BaseEntity> T findById(Class<T> entity, long recordId) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends BaseEntity> List<T> findAll(Class<T> entity) {
		return sessionFactory.openSession().createCriteria(entity).list();
	}
}
