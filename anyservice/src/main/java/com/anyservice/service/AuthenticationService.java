/**
 * 
 */
package com.anyservice.service;

import javax.servlet.http.HttpServletRequest;

import com.anyservice.dto.LoginDetails;

/**
 * @author DHIRAJ
 *
 */
public interface AuthenticationService {

	LoginDetails validateUser(String credentials, HttpServletRequest request);

}
