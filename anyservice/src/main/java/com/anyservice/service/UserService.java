package com.anyservice.service;

import com.anyservice.model.User;
import com.anyservice.model.UserInfo;

public interface UserService {

	UserInfo createUserInfo(String userName);

	User loginUser(String userName);

}
