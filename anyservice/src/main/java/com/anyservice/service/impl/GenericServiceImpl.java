package com.anyservice.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.anyservice.dao.BaseDao;
import com.anyservice.model.BaseEntity;
import com.anyservice.service.GenericService;

@Service
public class GenericServiceImpl implements GenericService {
	
	@Autowired
	private BaseDao baseDao;

	@Override
	public <T extends BaseEntity> T findByBusinessKey(Class<T> entity,
			Map<String, Object> param) {
		return baseDao.findByBusinessKey(entity, param);
	}

	@Override
	public <T extends BaseEntity> T findById(Class<T> entity, long recordId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T extends BaseEntity> List<T> findAll(Class<T> entity) {
		// TODO Auto-generated method stub
		return baseDao.findAll(entity);
	}

}
