package com.anyservice.service.impl;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.anyservice.dto.LoginDetails;
import com.anyservice.model.User;
import com.anyservice.model.UserInfo;
import com.anyservice.service.AuthenticationService;
import com.anyservice.service.UserService;
import com.anyservice.util.AnyServiceUtil;
import com.anyservice.util.CacheUtil;

@Component
@RequestMapping("/validate")
public class AuthenticationServiceImpl implements AuthenticationService {
	private static final String SALT_SEPERATOR = "\\|t=";
	private static final String MD5 = "MD5";
	private static final String _0 = "0";
	@Autowired
	private UserService userService;

	@Override
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody LoginDetails validateUser(
			@RequestBody String credentials, HttpServletRequest request) {
		Map<String, String> userMap = AnyServiceUtil.jsonToObject(credentials);
		LoginDetails loginStatus = new LoginDetails();
		String userName = userMap.get("userName");
		String encryptedPass = userMap.get("password");
		String[] keys1 = encryptedPass.split(SALT_SEPERATOR);
		try {
			String salt = userName + keys1[1];
			User user = userService.loginUser(userName);
			if (user == null) {
				throw new RuntimeException("INVALID_USER_CREDENTIALS");
			}
			String saltedPassword = user.getPassword() + salt;
			MessageDigest md = MessageDigest.getInstance(MD5);
			md.update(saltedPassword.getBytes(), 0, saltedPassword.length());
			String hashedPass = new BigInteger(1, md.digest()).toString(16);
			if (hashedPass.length() < 32) {
				hashedPass = _0 + hashedPass;
			}
			if (keys1[0].equals(hashedPass)) {
				loginStatus.setLoginStatus(true);
				UserInfo userInfo = userService.createUserInfo(userName);
				//loginStatus.setUserInfo(userInfo);
				HttpSession session = request.getSession();
				String userKey = session.getId();
				loginStatus.setUserKey(userKey);
				CacheUtil.putUserInfo("userInfo", userInfo);
				
				session.setAttribute("U.K.", userKey);
				session.setAttribute("userInfo", userInfo);
			}

		} catch (NoSuchAlgorithmException e) {
			loginStatus.setLoginStatus(false);
		}
		return loginStatus;

	}
}
