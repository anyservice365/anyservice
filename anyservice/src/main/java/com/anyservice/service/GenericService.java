package com.anyservice.service;

import java.util.List;
import java.util.Map;

import com.anyservice.model.BaseEntity;

public interface GenericService {

	<T extends BaseEntity> T findByBusinessKey(Class <T> entity, Map<String, Object> param);
	<T extends BaseEntity> T findById(Class<T> entity, long recordId);
	<T extends BaseEntity> List<T> findAll(Class<T> entity);

}
