package com.anyservice.dto;

import java.io.Serializable;

import com.anyservice.model.UserInfo;

public class LoginDetails implements Serializable {

	private static final long serialVersionUID = -3929748959569787953L;
	
	private boolean loginStatus;
	private String userKey;
	private UserInfo userInfo;

	public boolean isLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(boolean loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
}
