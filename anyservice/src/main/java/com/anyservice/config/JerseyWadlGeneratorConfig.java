package com.anyservice.config;

import java.util.List;

import org.glassfish.jersey.server.wadl.config.WadlGeneratorConfig;
import org.glassfish.jersey.server.wadl.config.WadlGeneratorDescription;
import org.glassfish.jersey.server.wadl.internal.generators.WadlGeneratorJAXBGrammarGenerator;

public class JerseyWadlGeneratorConfig extends WadlGeneratorConfig{

	@Override
	public List<WadlGeneratorDescription> configure() {
		// TODO Auto-generated method stub
		return generator(WadlGeneratorJAXBGrammarGenerator.class).descriptions();
	}

}
