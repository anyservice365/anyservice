var myApp = angular.module("seeker",[]);

myApp.controller("searchController", function($scope){
				$scope.myForm = { };
				$scope.modalShown = false;
				  $scope.toggleModal = function() {
				    $scope.modalShown = !$scope.modalShown;
				  };
				$scope.myForm.carOptions = [
					{ id : "Bangalore", name: "Bangalore" },
					{ id : "Delhi", name: "Delhi" },
					{ id : "Bhubaneswar"  , name: "Bhubaneswar" },
					{ id : "Bangalore", name: "Bangalore" },
					{ id : "Delhi", name: "Delhi" },
					{ id : "Bhubaneswar"  , name: "Bhubaneswar" },
					{ id : "Bangalore", name: "Bangalore" },
					{ id : "Delhi", name: "Delhi" },
					{ id : "Bhubaneswar"  , name: "Bhubaneswar" }
				];
				
				  $scope.services=[
							    { id : "Tutor", name: "Tutor" },
								{ id : "Fitness", name: "Fitness" },
								{ id : "Pet Care"  , name: "Pet Care" },
								{ id : "Parents", name: "Parents" },
								{ id : "Child care", name: "Child care" }        
							                ];
});

myApp.directive('modalDialog', function() {
  return {
    restrict: 'E',
    scope: {
      show: '='
    },
    replace: true, // Replace with the template below
    transclude: true, // we want to insert custom content inside the directive
    link: function(scope, element, attrs) {
      scope.dialogStyle = {};
      if (attrs.width)
        scope.dialogStyle.width = attrs.width;
      if (attrs.height)
        scope.dialogStyle.height = attrs.height;
      scope.hideModal = function() {
        scope.show = false;
      };
    },
    template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content' ng-transclude></div></div></div>"
  };
});

